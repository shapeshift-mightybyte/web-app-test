import logo from "./logo.svg";
import "./App.css";
import React, { useEffect, useState } from "react";

function App() {
  let color = "black";

  const [mnemonic, setMnemonic] = useState("");
  const [log, setLog] = useState("");

  const background = {
    backgroundColor: color,
  };

  const debugLog = (log: String) => {
    console.log(log);
  };

  useEffect(() => {
    const handleEvent = (message) => {
      console.log("We got something from the React Native App!");
      setLog("HELLO");
      setMnemonic(message.data.message);
    };
    document.addEventListener("message", handleEvent);
    return () => {
      document.removeEventListener("message", handleEvent);
    };
  });

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>This is the Web App!</p>
        <button
          onClick={() => {
            const dataJson = {
              color: "red",
            };
            window.ReactNativeWebView.postMessage(JSON.stringify(dataJson));
          }}
        >
          Communicate to Native App
        </button>
        <p>Mnemonic:</p>
        <p>{mnemonic}</p>
        <p>{log}</p>
      </header>
    </div>
  );
}

export default App;
